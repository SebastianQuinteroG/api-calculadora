﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplication1.Controllers
{
    [Route("calculadora")]
    [ApiController]
    public class CalculadoraController : ControllerBase
    {
        // GET: calculadora/<CalculadoraController>
        [HttpGet]
        public int Get()
        {
            return 0;
        }

        // GET calculadora/<CalculadoraController>/5
        [HttpGet("{id}")]
        public decimal Get(string id)
        {
            char[] spearator = { '|', '_' };
            string[] values = id.Contains("undefined") ? id.Remove(0,9).Split(spearator): id.Split(spearator);

            if (values.Length >= 2)
            {
                decimal value1 = Convert.ToDecimal(values[0]);
                decimal value2 = Convert.ToDecimal(values[1]);
                decimal answer = id.Contains('|') ? value1 + value2 : value1 - value2;
                return answer;
            }           
            
            return 0;
        }
    }
}
